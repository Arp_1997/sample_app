Rails.application.routes.draw do
  
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  root 'static_pages#home'    # Here we define the route, 'static_pages' is the controller name defined in 'app/controllers/concerns/static_pages_controller.rb'
                              # inside the controller we define an action in the form of a method 'home' , thus we reference the controller and its action as
                              # 'static_pages#home' , we pass this parameter to the roots method... 
                              # root("static_pages#home") 
  
  get  '/help',    to: 'static_pages#help'
  
  # It’s possible to use a named route other than the default using the as: option,you must also change the test file accordingly
  # get  '/help',    to: 'static_pages#help', as: 'helf'
  
  get    '/about',   to: 'static_pages#about'
  get    '/contact', to: 'static_pages#contact'
  get    '/signup',  to: 'users#new'
  post   '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  
                                                                                # RESTful RESOURCES , they help in generating named routes for generating user URLs for various actions
                                                        
                                                          
                                                                                # ACTION      HTTP request         Named route                        Purpose
                                                                                # -------------------------------------------------------------------------------------------------------------------
  resources :users                                                              # index:          GET              users_path	                        => page to list all users
                                                                                # show:           GET              user_path(user)	                  => page to show user / user profile
                                                                                # new:            GET              new_user_path	                    => page to make a new user (signup)
                                                                                # create:         POST             users_path	                        => create a new user in the db after signup
                                                                                # edit:           GET              edit_user_path(user)	              => page to edit user with id 1
                                                                                # update:         PATCH	           user_path(user)	                  => when user update's user details
                                                                                # destroy         DELETE           destroy	user_path(user)	          => WHen a Admin user deletes other users
  
 
  resources :account_activations, only: [:edit]                                 # edit:           GET	             edit_account_activation_url(token) => when user signup's generate activation url

  
  resources :password_resets,     only: [:new, :create, :edit, :update]         # new:            GET              new_password_reset_path            => when user clicks on forgot password link 
                                                                                # create:         POST             password_resets_path               => create password reset link 
                                                                                # edit:           GET              edit_password_reset_url(token)     => when user tries to reset password using reset link
                                                                                # update:         PATCH            password_reset_path(token)         => when users tries to update wih new password
                                                                                
  resources :microposts,          only: [:create, :destroy]                     # create          POST	           microposts_path                    => when a user creates a new micropost
                                                                                # destroy         DELETE	         micropost_path(micropost)          => when a micropost is deleted by an user
  
  
  resources :relationships,       only: [:create, :destroy]                     # create          POST                                                => when user follows another user create a new relationship 
                                                                                #                                                                     => ( Creates a new record in the relationship table with [:follower_id, :followed_id]           
                                                                                # destroy         DELETE                                              => when user unfollows another user delete their corresponding relationship from the relationships table
  
  # The URLs for following and followers will look like 
  # /users/1/following and /users/1/followers. Since both
  # pages will be showing data, the proper HTTP verb is a GET request,
  # so we use the get method to arrange for the URLs to respond appropriately.
  # Meanwhile, the member method arranges for the routes 
  # to respond to URLs containing the user id.
  
  resources :users do                                                           # following       GET	        following	following_user_path(user_id)  => when user clicks on following a GET request 
                                                                                #                                                                        generates a page for That user listing all other
    member do     # For every user ID member 
                  # method arranges for the routes 
                  # to respond to URLs containing the user id.                  #                                                                        users he is following
      get :following, :followers                                                # followers       GET	        followers	followers_user_path(user_id)  => when user clicks on followers a GET request 
    end                                                                         #                                                                        generates a page for That user listing all his followers
  
  end                                                                           


  resources :microposts do
    resources :likes
    resources :dislikes
    resources :hearts
    end
  
   
  
end

# _path helpers provide a site-root-relative path. You should probably use this most of the time. Eg: /password_resets

# _url helpers provide an absolute path, including protocol and server name. Eg: http://ex.co/password_resets/<token>/edit