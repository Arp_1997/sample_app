# skip image resizing in test environment, that is skip image resizing in tests

if Rails.env.test?
  CarrierWave.configure do |config|
    config.enable_processing = false
  end
end