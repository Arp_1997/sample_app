source 'https://rubygems.org'              # Get all ruby gems here

gem 'rails',                   '5.1.6'     # RAILS 5.1.6
gem 'bcrypt',                  '3.1.12'    # Used for encrypting passwords before saving to db, also used for generating and verifying activation and password digests
gem 'faker',                   '1.7.3'     # Used to generate users with random names and random lorem sentences for microposts in fixtures for testing purposes
gem 'carrierwave',             '1.2.2'     # Image uploader used to handle uploading images as microposts for users
gem 'mini_magick',             '4.7.0'     # Manipulate images with minimal use of memory via ImageMagick
gem 'will_paginate',           '3.1.6'     # Used for implemeneting pagination that is show content in pages ( in index and microposts), 
                                           # will_paginate provides a simple API for performing paginated queries with Active Record
gem 'bootstrap-will_paginate', '1.0.0'     # Hooks into will_paginate to format the html to match Twitter Bootstrap styling
gem 'puma',                    '3.9.1'     # Puma is a simple, fast, threaded, and highly concurrent HTTP 1.1 server for Ruby/Rack applications, it is intended for use in both development and production environments. 
gem 'sass-rails',              '5.0.6'     # Sass adapter for the Rails asset pipeline, inpret scss files to css files in the asset pipeline 
gem 'uglifier',                '3.2.0'     # Uglifier minifies JavaScript files by wrapping UglifyJS to be accessible in Ruby
gem 'coffee-rails',            '4.2.2'     # CoffeeScript adapter for the Rails asset pipeline. ( CoffeeScript is a programming language that transcompiles to JavaScript. It adds syntactic sugar inspired by Ruby, Python and Haskell in an effort to enhance JavaScript's brevity and readability.)
gem 'jquery-rails',            '4.3.1'     # This gem provides jQuery and the jQuery-ujs driver for your Rails 4+ application.
gem 'turbolinks',              '5.0.1'     # Rails engine for Turbolinks 5 support ( Turbolinks is a Rails feature, available as a gem and enabled by default in new Rails apps. It is intended to speed up navigating between pages of your application. It works by intercepting all link clicks that would navigate to a page within the app, and instead makes the request via AJAX, replacing the body with the received content. The primary speedup comes from not having to download or parse the CSS & JS files again.)
gem 'jbuilder',                '2.7.0'     # Create JSON structures via a Builder-style DSL
gem 'bootstrap-sass',          '3.3.7'     # bootstrap-sass is a Sass-powered version of Bootstrap 3

group :development, :test do
  gem 'sqlite3', '1.3.13'                  # This module allows Ruby programs to interface with the SQLite3 database engine 
  gem 'byebug',  '9.0.6', platform: :mri   # Byebug is a Ruby debugger
end

group :development do
  gem 'web-console',           '3.5.1'     # A debugging tool for your Ruby on Rails applications.
  gem 'listen',                '3.1.5'     # The Listen gem listens to file modifications and notifies you about the changes.
  gem 'spring',                '2.0.2'     # Preloads your application so things like console, rake and tests run faster
  gem 'spring-watcher-listen', '2.0.1'     # Makes spring watch files using the listen gem.
end

group :test do
  gem 'rails-controller-testing', '1.0.2'  # Extracting `assigns` and `assert_template` from ActionDispatch.
  gem 'minitest',                 '5.10.3' # minitest provides a complete suite of testing facilities supporting TDD, BDD, mocking, and benchmarking.
  gem 'minitest-reporters',       '1.1.14' # Extend Minitest through simple hooks.
  gem 'guard',                    '2.14.1' # Guard is a command line tool to easily handle events on file system modifications.
  gem 'guard-minitest',           '2.4.6'  # Guard::Minitest automatically run your tests with Minitest framework (much like autotest)
end

group :production do
  gem 'pg',  '0.20.0'                      # Pg is the Ruby interface to the {PostgreSQL RDBMS}
  gem 'fog', '1.42'                        # The Ruby cloud services library. Supports all major cloud providers including AWS, Rackspace, Linode, Blue Box, StormOnDemand, and many others. 
                                           # Full support for most AWS services including EC2, S3, CloudWatch, SimpleDB, ELB, and RDS.
                                           # It is used to store images uploaded as microposts by user
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]              # TZInfo::Data contains data from the IANA Time Zone database packaged as Ruby modules for use with TZInfo.


