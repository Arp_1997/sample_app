require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:michael)
    remember(@user) # keep the users remeber_token in databse
  end

  test "current_user returns right user when session is nil" do   # This will test the case when a user opens the website and is not logged in so session is nil
                                                                  # but has a cookie from his previous login, thus he will be logged in so is_logged_in? returns true
                                                                  # and the @user = current user that is logged in (via cookie from previous login)
    assert_equal @user, current_user
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do  # This will test the case when a user opens the website and is not logged in so session is nil
                                                                    # but has a cookie with wrong remember_digest thus user.authenticated? returns false
                                                                    # , thus he will not be logged in so current_user is nil
    
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
  
end