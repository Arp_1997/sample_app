require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "Example User", email: "user@example.com", password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  
  # unlike asset , the assertion assert_not will pass only if the value to the assertion is false
  # This is usefull when checking invalid names or emails for which the valid? method MUST return false
  
  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test "associated microposts should be destroyed" do
    @user.save                                              # Save the user to the database
    @user.microposts.create!(content: "Lorem ipsum")        # Create a micropost for the user and store it to databse by @user.microposts.create!() method
    assert_difference 'Micropost.count', -1 do              # Check that the user's micropost was deleted when the user was removed from the database
      @user.destroy                                         # This is done by checking that destroying the user reduces the micropost count by 1
    end
  end 
  
  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)                   # Verify "michael" is not following "archer"
    michael.follow(archer)                                  # set "michael" to follow "archer"
    assert michael.following?(archer)                       # Verify "michael" is now following "archer" 
    assert archer.followers.include?(michael)               # Verify "arhcer's" followers includes "michael"
    michael.unfollow(archer)                                # set "michael" to unfollow "archer"
    assert_not michael.following?(archer)                   # Verify "michael" has unfollowed "archer"
  end
  
  # We have arranged for Michael to follow Lana but not Archer; based on the fixtures
  # This means that Michael should see Lana’s posts and his own posts, but not Archer’s posts.
  # This is tested in the following tests
  
  
  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    # Posts from followed user
    lana.microposts.each do |post_following|        # Cycle through Lana's microposts
      assert michael.feed.include?(post_following)  # and verify michael's feed has those microposts
    end
    # Posts from self
    michael.microposts.each do |post_self|          # Cycle through Michael's own microposts
      assert michael.feed.include?(post_self)       # and verify michael's feed has those microposts
    end
    # Posts from unfollowed user
    archer.microposts.each do |post_unfollowed|         # Cycle through Archer's microposts
      assert_not michael.feed.include?(post_unfollowed) # and verify michael's feed does not have those microposts
    end
  end

end