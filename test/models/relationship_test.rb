require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase

  def setup
    @relationship = Relationship.new(follower_id: users(:michael).id,
                                     followed_id: users(:archer).id)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?       # Validates a relationship must have a follower_id
  end

  test "should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?       # Validates a relationship must have a followed_id
  end
end