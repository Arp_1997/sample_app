require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end

  test "password resets" do
    get new_password_reset_path                                                 # Goto password reset path (as if we clicked on forgot password)
    assert_template 'password_resets/new'                                       # Check if the password reset page opened correctly
    # Invalid email
    post password_resets_path, params: { password_reset: { email: "" } }        # Try to enter an invalid email (empty "") and submit form
    assert_not flash.empty?                                                     # Check that an error flash message is displayed
    assert_template 'password_resets/new'                                       # Check if the password reset page is open (that is it is refreshed to show flash message)
    # Valid email
    post password_resets_path, params: { password_reset:{ email: @user.email}}  # provide a valid email id and submit form, that is trying a post request on password reset path

    assert_not_equal @user.reset_digest, @user.reload.reset_digest              # Verify that a new password reset digest is created ( before it was nil now it is something)
    assert_equal 1, ActionMailer::Base.deliveries.size                          # verify password reset mail was sent ( taht is array size of deliveries.size  is 1)
    assert_not flash.empty?                                                     # There should be a flash message indicating that password reset mail was sent
    assert_redirected_to root_url                                               # redirect to home page
    # Password reset form
    user = assigns(:user)                                                       # use assign() method to use the @user instance variable defined within the controller
    # Wrong email
    get edit_password_reset_path(user.reset_token, email: "")                   # On trying to open the password reset path with a wrong email
    assert_redirected_to root_url                                               # we are redirected to home page
    # Inactive user
    user.toggle!(:activated)                                                    # Set activated = false for the user, that is user is not activated
    get edit_password_reset_path(user.reset_token, email: user.email)           # On trying to open the password reset path for the inactivated user with valid reset_token and email
    assert_redirected_to root_url                                               # check if we are correctly redirected to home page in this case
    user.toggle!(:activated)                                                    # Set activated = true again for the user
    # Right email, wrong token  
    get edit_password_reset_path('wrong token', email: user.email)              # On trying to open the password reset path with a right email but a wrong token
    assert_redirected_to root_url                                               # check if we are correctly redirected to home page in this case
    # Right email, right token
    get edit_password_reset_path(user.reset_token, email: user.email)           # On trying to open the password reset path with a right email and right token
    assert_template 'password_resets/edit'                                      # check password reset edit page has now opened successfully
    assert_select "input[name=email][type=hidden][value=?]", user.email         # Check that there is an input tag with the right name, (hidden) type, and email address
    # Invalid password & confirmation
    patch password_reset_path(user.reset_token),                                # Enter a new password but a wrong password confirmation (here you are updating the password so its a patch request)
          params: { email: user.email,
                    user: { password:              "foobaz",
                            password_confirmation: "barquux" } }
    assert_select 'div#error_explanation'                                       # Check that the div with id='error_explanation' has appeared
    # Empty password
    patch password_reset_path(user.reset_token),                                # Enter a new EMPTY password
          params: { email: user.email,
                    user: { password:              "",
                            password_confirmation: "" } }
    assert_select 'div#error_explanation'                                       # Check that the div with id='error_explanation' has appeared
    # Valid password & confirmation
    patch password_reset_path(user.reset_token),                                # Enter a valid new password and password confirmation
          params: { email: user.email,
                    user: { password:              "foobaz",
                            password_confirmation: "foobaz" } }
    assert is_logged_in?                                                        # Check that the user is logged in after successfully updating password
    assert_not flash.empty?                                                     # Check a success message was shown
    assert_redirected_to user                                                   # Check the user was redirected to his profile
    assert_nil user.reload.reset_digest                                         # Check after updating password, the word reset digest is correctly reset to nil to avoid malicious use
                                                                                # Here we must reload the user data from the database before checking
  end
  
  
  test "expired token" do
    get new_password_reset_path                                                 # Go to the password reset page
    post password_resets_path,params: {password_reset:{ email: @user.email }}   # provide a valid email id and submit form, that is trying a post request on password reset path

    @user = assigns(:user)                                                      # use assign() method to use the @user instance variable defined within the controller ( For a test to access the controller variable it must use the assign() method 
    @user.update_attribute(:reset_sent_at, 3.hours.ago)                         # Update the reset sent at attribute to 3 hours so that it expires
    patch password_reset_path(@user.reset_token),                               # Enter a new password but a wrong password confirmation (here you are updating the password so its a patch request)
          params: { email: @user.email,
                    user: { password:              "foobar",
                            password_confirmation: "foobar" } }
    assert_response :redirect                                                   # Check if you are redirected
    follow_redirect!                                                            # Follow the redirection
    assert_match (/expired/), response.body                                     # Check that a message was shown that contains the word 'expired' in the body
                                                                                # the code 'response.body', will  return the full HTML body of the page
  end
  
  
  
  
end