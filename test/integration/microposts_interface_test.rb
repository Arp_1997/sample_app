# test to log in, check the micropost pagination, 
# make an invalid submission, make a valid submission, delete a post, 
# and then visit a second user’s page to make sure there are no “delete” links. 

require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "micropost interface" do
    log_in_as(@user)                                                            # Login @user = users(:michael)
    get root_path                                                               # Get to home
    assert_select 'div.pagination'                                              # check <div> tag with class="pagination" exits
    assert_select 'input[type=file]'                                            # check for a file upload field to upload image for micropost
    # Invalid submission
    assert_no_difference 'Micropost.count' do                                   # Check proper validation of microposts exits by trying to submit a micropost with empty content
      post microposts_path, params: { micropost: { content: "" } }              # and verifying Micropost.count did not change
    end
    assert_select 'div#error_explanation'                                       # On trying to post a invalid (empty) micropost check that proper error message appeared 
                                                                                # as a <div> tag with class="error_explanation"
    # Valid submission
    content = "This micropost really ties the room together"  
    picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')       # fixture_file_upload method IS USED for uploading files as fixtures inside tests
                                                                                # Here it is used to upload an image as part of a micropost
    assert_difference 'Micropost.count', 1 do                                   # Check on submitting a valid micropost the Micropost.count changed by +1
    post microposts_path, params: { micropost:                                  # Post a micropost with the content and picture sent as a params
                                      { content: content,
                                        picture: picture } }
    end
    assert_redirected_to root_url                                               # Check user is redirected to home on submitting valid microposts from home 
                                                                                # this happens due to redirect_to request.referrer
    follow_redirect!                                                            # Follow the redirection
    assert_match content, response.body
    # Delete post
    assert_select 'a', text: 'delete'                                           # Check a <a> tag exists with text "delete"
    first_micropost = @user.microposts.paginate(page: 1).first                  # Select @user michael's first micropost
    assert_difference 'Micropost.count', -1 do                                  # Verify the micropost count changed by -1 on deletion of the selected micropost
      delete micropost_path(first_micropost)
    end
    # Visit different user (no delete links)
    get user_path(users(:archer))                                               # Visit a diffrent user's profile 
    assert_select 'a', text: 'delete', count: 0                                 # verify that the option to delete micropost doesnot appear when viewing micropost's
                                                                                # of a different user in his profile, this is done by checking                                                                                # there is no <a> tag with text "delete"
  end
  
  test "micropost sidebar count" do                                             # This tests that the micropost count for a user on the HOME page is correct
    log_in_as(@user)                                                            # Login @user = users(:michael)
    get root_path                                                               # Get to home
    assert_match "#{@user.microposts.count} microposts", response.body          # Check microposts count is displayed properly in html body                     
    # User with zero microposts
    other_user = users(:malory)                                                  
    log_in_as(other_user)                                                       # Login @user = users(:malory)        
    get root_path                                                               # Get to home
    assert_match "0 microposts", response.body                                  # Check that malory has 0 microposts
    other_user.microposts.create!(content: "A micropost")                       # Create a micropost for malory
    get root_path                                                               # Get to home (refresh page to see updated microposts count) 
    assert_match "1 micropost", response.body                                   # Check malory now has 1 micropost
                                                                                # Note since malory now has 1 'micropost' it correctly says micropost and not 'microposts'
                                                                                # This is due to ' pluralize(current_user.microposts.count, "micropost") ' in '_user_info.html.erb'

  end
  
end