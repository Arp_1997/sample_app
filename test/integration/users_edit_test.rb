require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }

    assert_template 'users/edit'
    
    assert_select "div.alert", "The form contains 4 errors."
    
  end
  
   test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end
  
  
  test "successful edit with friendly forwarding" do  # If a non-logged in user tries to access edit page he will be redirected to login page
                                                      # Now test after he logs in he must be redirected to his edit page instead of his profile page
                                                      
    get edit_user_path(@user)                   # visit editing page in non-logged in state
    log_in_as(@user)                            # login
    assert_redirected_to edit_user_url(@user)   # check if user is redirected to editing page
    
    # make edits
    
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
    
    assert_not session[:forwarding_url] # check that after forwarding the forward_url is deleted from the session
  end
  
  
  
end