require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  
  include ApplicationHelper   # To the use of the full_title helper to test the page’s title, 
                              # which we gain access to by including the Application Helper module in app/helpers/application_helper.rb into the test
  
  def setup
    @user = users(:michael)
  end

  test "profile display" do
    get user_path(@user)                                                        # Go to user's profile
    assert_template 'users/show'                                                # Verify that user's profile page has openend correctly
    assert_select 'title', full_title(@user.name)                               # Verify Correct page title is shown
    assert_select 'h1', text: @user.name                                        # Verify a <h1> tag with user's name is present
    assert_select 'h1>img.gravatar'                                             # Verify a <img> tag with class="gravatar" exists within a <h1> tag
    assert_match @user.microposts.count.to_s, response.body                     # Check html body contains the count of number of user's microposts
    assert_select 'div.pagination'                                              # Check proper pagination exists
    assert_select 'div.pagination', count: 1                                    # test that will_paginate appears only once
    
    @user.microposts.paginate(page: 1).each do |micropost|                      # Check all the microposts in the first page have their contenet in the html body
      assert_match micropost.content, response.body
    end
  end
  
  test "home page stats" do
    log_in_as(@user)                                                            # Login User
    get root_path                                                               # Go to user's home
    assert_select 'div.stats'                                                   # Check we have a <div> with class="stats"
    assert_select '#following' , text: @user.following.count.to_s               # Check the follower count is present in the element with id="following"
    assert_select '#followers' , text: @user.followers.count.to_s               # Check the following count is present in the element with id="followers"
  end
end