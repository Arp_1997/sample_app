require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @admin     = users(:michael)
    @non_admin = users(:archer)
  end

  test "index including pagination" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination' , count:2
    User.paginate(page: 1).each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name   # tests that the first page of the pagination has users with links to their profile and their name displayed properly
    end
  end
  
  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      
      unless user == @admin                                           # Check if the delete links appear for all users except fot the current admin user
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
      
    end
    assert_difference 'User.count', -1 do                 # check that the user count reduces by 1 after deleting an user
      delete user_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0         # check for a non-admin user their are no delete user links appearing in the index page
  end
  
  
end