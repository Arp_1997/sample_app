require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael) # Here users corresponds to the fixture filename users.yml, while the symbol :michael references user with the key 'michael'
  end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", signup_path
    
    get contact_path
    assert_select "title", full_title("Contact")
    
    assert_select "a[href=?]", users_path, count: 0 # the index page link must not be visible to non-logged in users
    
    log_in_as(@user)
    
    get root_path # refresh home page to check if index page link has appeared after logging in
                                          
    assert_select "a[href=?]", users_path # the index page link must be visible to user after logging in                                   
     

  end
end
