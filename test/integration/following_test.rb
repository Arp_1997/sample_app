require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other = users(:archer)
    log_in_as(@user)
  end

  test "following page" do
    get following_user_path(@user)                                              # Go to followings page for the @user = users(:michael)
    assert_not @user.following.empty?                                           # Check the user has followings that is @user.following array is not empty
                                                                                # this is necessary because if @user.following.empty? were true, 
                                                                                # not a single assert_select would execute in the following loop, 
                                                                                # leading the tests to pass and thereby give us a false sense of security.
    assert_match @user.following.count.to_s, response.body                      # Check followings count is present in HTML body
    @user.following.each do |user|                                              # For each following of @user
      assert_select "a[href=?]", user_path(user)                                # Verify there exists a link to that following's profile path
    end
  end

  test "followers page" do
    get followers_user_path(@user)                                              # Go to followers page for the @user = users(:michael)
    assert_not @user.followers.empty?                                           # Check the user has followers that is @user.followers array is not empty
    assert_match @user.followers.count.to_s, response.body                      # Check followers count is present in HTML body
    @user.followers.each do |user|                                              # For each follower of @user
      assert_select "a[href=?]", user_path(user)                                # Verify there exists a link to that follower's profile path
    end
  end
  
  test "should follow a user the standard way" do                               # Tries to follow user without ajax which needs to refresh page on 
    assert_difference '@user.following.count', 1 do                             # following/unfollowing user to update page componenets (usefull on browsers where javascript is disabled)
      post relationships_path, params: { followed_id: @other.id }               # Issues a POST request with {followe_id: @other.id} as a param Hash
    end                                                                         # Verifys that the user's follower count was updated
  end

  test "should follow a user with Ajax" do                                      # Tries to follow user and uses ajax request to update page components (the follow/unfollow button and followe count)
    assert_difference '@user.following.count', 1 do                             # Verifys that the user's follower count was updated
      post relationships_path, xhr: true, params: { followed_id: @other.id }    # Issues a POST request with {followe_id: @other.id} as a param Hash
    end                                                                         # Here xhr stands for XmlHttpRequest; setting the xhr option to true issues an Ajax request in the test
  end

  test "should unfollow a user the standard way" do                             # Tries to unfollow user without ajax which needs to refresh page on 
                                                                                # following/unfollowing user to update page componenets (usefull on browsers where javascript is disabled)
    @user.follow(@other)                                                        # First follows user                                   
    relationship = @user.active_relationships.find_by(followed_id: @other.id)   # Finds following relationship  
    assert_difference '@user.following.count', -1 do                            # Verifys the user's follower count was updated                     
      delete relationship_path(relationship)                                    # Then issues a DELETE request to unfollow user by deleting corresponding relationship record 
    end
  end

  test "should unfollow a user with Ajax" do                                    # Tries to unfollow user and uses ajax request to update page components (the follow/unfollow button and followe count)
    @user.follow(@other)                                                        # First follows user
    relationship = @user.active_relationships.find_by(followed_id: @other.id)   # Finds following relationship
    assert_difference '@user.following.count', -1 do                            # Verifys that the user's follower count was updated
      delete relationship_path(relationship), xhr: true                         # Then issues a DELETE request to unfollow user by deleting corresponding relationship record 
    end                                                                         # Here xhr stands for XmlHttpRequest; setting the xhr option to true issues an Ajax request in the test
  end
  
  test "use cannot unfollow admin" do                                           # Users cannot unfollow admin
   get signup_path                                                              # First signup with valid data
    assert_difference 'User.count', 1 do
    post users_path, params: { user: { name:  "Example User",
                                       email: "user@example.com",
                                       password:              "password",
                                       password_confirmation: "password" } }
    end
      
    user = assigns(:user)                                                       # assigns lets us access instance variables in the corresponding action
                                                                                # Users controller’s create action defines an @user variable, so we can 
    arpan = User.find_by email: "arpanghoshal3@gmail.com"                       # Get admin user
    user.unfollow(arpan)                                                        # Try to unfollow admin
    assert user.following?(arpan)                                               # Verify user is still following admin
  end
  
end