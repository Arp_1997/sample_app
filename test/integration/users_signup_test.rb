require 'test_helper'



class UsersSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear # Because the deliveries array is global, we have to reset it in the setup method to prevent our code from breaking if any other tests deliver email 
  end
  
    
  test "invalid signup information" do
    
    get signup_path  # Visit the sign up page, this is optional
    
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    
    assert_template 'users/new'   # assert_template to check that a failed submission re-renders the new action
    
    assert_select 'div#error_explanation' # check if the corresponding divs appear in case of incorrect submission
    assert_select 'div.alert-danger'
    
  end
  
  test "valid signup information with account activation" do
    
    get signup_path
    assert_difference 'User.count', 1 do
    post users_path, params: { user: { name:  "Example User",
                                       email: "user@example.com",
                                       password:              "password",
                                       password_confirmation: "password" } }
    end
    
    assert_equal 1, ActionMailer::Base.deliveries.size  # This code verifies that exactly 1 message was delivered by checking the size ofthe deliveries array
    
    user = assigns(:user) # assigns lets us access instance variables in the corresponding action
                          # Users controller’s create action defines an @user variable, so we can access it in the test using assigns(:user)
    
    assert_not user.activated?
    # Try to log in before activation.
    log_in_as(user)
    assert_not is_logged_in?
    # Invalid activation token
    get edit_account_activation_path("invalid token", email: user.email)
    assert_not is_logged_in?
    # Valid token, wrong email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # Valid activation token
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
  
    
  test "user should follow admin on signup" do
    get signup_path                                                             # First signup with valid data
    assert_difference 'User.count', 1 do
    post users_path, params: { user: { name:  "Example User",
                                       email: "user@example.com",
                                       password:              "password",
                                       password_confirmation: "password" } }
    end
      
    user = assigns(:user)                                                       # assigns lets us access instance variables in the corresponding action
                                                                                # Users controller’s create action defines an @user variable, so we can 
                                                                                # access it in the test using assigns(:user)  
    arpan = User.find_by email: "arpanghoshal3@gmail.com"                       # Find the admin user using his email
    assert user.following?(arpan)                                               # Verify the user created is following the admin user
    
  end
  

  

end
