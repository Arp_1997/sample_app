require 'test_helper'

class UserMailerTest < ActionMailer::TestCase

  test "account_activation" do
    user = users(:michael)                                    # Select the user "michael"
    user.activation_token = User.new_token                    # Generate a new activation token for the user
    mail = UserMailer.account_activation(user)                # Pass the user to account_activation(user) which returns the activation mail sent to the user
    assert_equal "Account activation", mail.subject           # Check the mails subject is "Account activation"
    assert_equal [user.email], mail.to                        # Check the mail is sent to the users mail ID
    assert_equal ["noreply@example.com"], mail.from           # Check the mail is from "noreply@example.com"
    assert_match user.name,               mail.body.encoded   # Check the mail's body contains the user's name
    assert_match user.activation_token,   mail.body.encoded   # Check the mail's body contains the user's activation_token
    assert_match CGI.escape(user.email),  mail.body.encoded   # Check the mail's body contains the user's mail address
  end

  test "password_reset" do
    user = users(:michael)
    user.reset_token = User.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "Password reset", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match user.reset_token,        mail.body.encoded
    assert_match CGI.escape(user.email),  mail.body.encoded
  end
end