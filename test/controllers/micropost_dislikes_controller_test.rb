require 'test_helper'

class MicropostDislikesControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get micropost_dislikes_create_url
    assert_response :success
  end

  test "should get destroy" do
    get micropost_dislikes_destroy_url
    assert_response :success
  end

end
