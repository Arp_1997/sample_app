require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @micropost = microposts(:orange)
  end

  test "should redirect create when not logged in" do                           # On trying to CREATE microposts without logging in the 'Micropost.count' should not change
    assert_no_difference 'Micropost.count' do                                   
      post microposts_path, params: { micropost: { content: "Lorem ipsum" } }   # POST request to create micropost
    end
    assert_redirected_to login_url                                              # and we must instead be redirected to login url
  end

  test "should redirect destroy when not logged in" do                          # On trying to DELETE microposts without logging in the 'Micropost.count' should not change
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@micropost)                                         # DELETE request to destroy micropost
    end
    assert_redirected_to login_url                                              # and we must instead be redirected to login url
  end
  
  test "should redirect destroy for wrong micropost" do
    log_in_as(users(:michael))
    micropost = microposts(:ants)                                               # Select all microposts by the user "ant" (present in fixture microposts.yml)
    assert_no_difference 'Micropost.count' do                                   # ensure that user "michael" cannot delete microposts of user "ant" by checking
      delete micropost_path(micropost)                                          # Micropost.count does not change
    end
    assert_redirected_to root_url                                               # Check that trying to do this will redirect the user to his profile page
  end
end