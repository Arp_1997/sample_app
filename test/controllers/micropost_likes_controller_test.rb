require 'test_helper'

class MicropostLikesControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get micropost_likes_create_url
    assert_response :success
  end

  test "should get destroy" do
    get micropost_likes_destroy_url
    assert_response :success
  end

end
