require 'test_helper'

class MicropostLovesControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get micropost_loves_create_url
    assert_response :success
  end

  test "should get destroy" do
    get micropost_loves_destroy_url
    assert_response :success
  end

end
