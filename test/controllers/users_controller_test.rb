require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end
  
  
  test "should get new" do
    get signup_path
    assert_response :success
  end
  
  test "should redirect edit when not logged in" do   # If user is not logged in trying to access edit path will redirect to login page with error message via flash
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do   # If user is not logged in trying to update user data will redirect to login page with error message via flash
    get edit_user_path(@user)
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should redirect index when not logged in" do    # If non-logged in user trys to access index page redirect to login page
    get users_path
    assert_redirected_to login_url
  end
  
  
  # if we simply passed an initialization hash in from an arbitrary web request, a malicious user could send a PATCH request as 'patch /users/17?admin=1'
  # This is avoided by permitting update attributes that are safe to edit through the web in the user_params() method in user_controller
  # This test will test this behaviour
  test "should not allow the admin attribute to be edited via the web" do 
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user), params: {
                                    user: { password:              'password',
                                            password_confirmation: 'password',
                                            admin: true } }                       # Try to set the user as an admin by sending a PATCH request with admin: true
                                            
    assert_not @other_user.reload.admin?  # Reload the user data from db and ensure that the user was not made admin this way
  end
  
  
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do    # check that non-logged in users cannot perform delete operations
      delete user_path(@user)
    end
    assert_redirected_to login_url          # check that non-logged in users will be redirected to login page on trying to issue delete operation
  end
  
  

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)                  
    assert_no_difference 'User.count' do  # check that non-admin users cannot perform delete operations 
                                          # this is due to 'before_action :admin_user, only: :destroy' in user_controller.rb 
      delete user_path(@user)
    end
    assert_redirected_to root_url         # check that non-admin users will be redirected to home page on trying to issue delete operation
  end
  
  test "should redirect following when not logged in" do
    get following_user_path(@user)                        # Try to go the followings page for @user without logging in
    assert_redirected_to login_url                        # Verify that you are redirected to login_url in this case
  end

  test "should redirect followers when not logged in" do
    get followers_user_path(@user)                        # Try to go the followers page for @user without logging in
    assert_redirected_to login_url                        # Verify that you are redirected to login_url in this case
  end
  

end
