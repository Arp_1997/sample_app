require 'test_helper'

class RelationshipsControllerTest < ActionDispatch::IntegrationTest

  test "create should require logged-in user" do
    assert_no_difference 'Relationship.count' do                                # No relations should be created when user is not logged in
      post relationships_path                                                   # Try the create a new relation using a POST request
    end
    assert_redirected_to login_url
  end

  test "destroy should require logged-in user" do                               
    assert_no_difference 'Relationship.count' do                                # No relations should be destroyed when user is not logged in
      delete relationship_path(relationships(:one))                             # Try the destroy a new relation using a DELETE request
    end
    assert_redirected_to login_url
  end
end