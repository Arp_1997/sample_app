# Use's the faker gem to seed the database with 100 users

User.create!(name:  "Arpan (admin)",
             email: "arpanghoshal3@gmail.com",
             password:              "password",
             password_confirmation: "password",
             admin: true,
             activated: true,               # User's account is activated
             activated_at: Time.zone.now)   # Account activation time

#99.times do |n|
#  name  = Faker::Name.name  # generate a sample name
#  email = "example-#{n+1}@railstutorial.org"
#  password = "password"
#  User.create!(name:  name,
#               email: email,
#               password:              password,
#               password_confirmation: password,
#               activated: true,
#               activated_at: Time.zone.now)
#end


# The folowing code generates a micropost and adds it to each user, this process is repeated 50 times so that each user has 50 microposts

# users = User.order(:created_at).take(6)     # Take the first 6 users ordered by their creation time
# 50.times do                                 # loop 50 times
#   content = Faker::Lorem.sentence(5)        # To generate sample content for each micropost, we’ll use the Faker gem’s handy Lorem.sentence method.
#   users.each { |user| user.microposts.create!(content: content) }   # For each user add the generated micropost content
# end

# Following relationships
# users = User.all
# user  = users.first
# following = users[2..50]
# followers = users[3..40]
# following.each { |followed| user.follow(followed) } # Make the first user follow users 2 to 50
# followers.each { |follower| follower.follow(user) } # Make users 3 to 40 follow the first user