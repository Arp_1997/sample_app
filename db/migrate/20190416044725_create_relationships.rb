class CreateRelationships < ActiveRecord::Migration[5.1]
  def change
    create_table :relationships do |t|
      t.integer :follower_id
      t.integer :followed_id

      t.timestamps
    end
    
    # Because we will be finding relationships by follower_id and by followed_id, we should add an index on each column for efficiency
    
    add_index :relationships, :follower_id                                      # Add index to follower_id, to make search by follower_id faster
    add_index :relationships, :followed_id                                      # Add index to followed_id, to make search by followed_id faster
    add_index :relationships, [:follower_id, :followed_id], unique: true        # This a multiple-key index that enforces uniqueness on (follower_id, followed_id) pairs, 
                                                                                # so that a user can’t follow another user more than once.
    
  end
end
