class CreateHearts < ActiveRecord::Migration[5.1]
  def change
    create_table :hearts do |t|
      t.references :micropost
      t.references :user

      t.timestamps
    end
    
      add_index :hearts, [:user_id, :micropost_id] 
  end
end
