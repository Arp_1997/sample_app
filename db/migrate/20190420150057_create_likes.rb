class CreateLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :likes do |t|
      t.references :micropost
      t.references :user

      t.timestamps
    end
    
      add_index :likes, [:user_id, :micropost_id] 
    
  end
end
