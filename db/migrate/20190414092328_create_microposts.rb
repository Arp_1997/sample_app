class CreateMicroposts < ActiveRecord::Migration[5.1]
  def change
    create_table :microposts do |t|
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
    
    add_index :microposts, [:user_id, :created_at] # Because we expect to retrieve all the microposts associated with a given user id in reverse order of creation
                                                   # this will add an index on the user_id and created_at columns,By including both the user_id and created_at columns 
                                                   # as an array, we arrange for Rails to create a multiple key index, which means that Active Record uses both keys at the same time
  end
end
