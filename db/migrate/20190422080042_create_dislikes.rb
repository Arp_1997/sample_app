class CreateDislikes < ActiveRecord::Migration[5.1]
  def change
    create_table :dislikes do |t|
      t.references :micropost
      t.references :user

      t.timestamps
    end
       
      add_index :dislikes, [:user_id, :micropost_id] 
    
  end

end
