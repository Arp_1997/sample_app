class PictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  process resize_to_limit: [400, 400]
  
  # Here we use the production? boolean to switch storage method based on the environment
  # Rails comes equipped with three environments: test, development, and production. The default environment for the Rails console is development
  
 

  if Rails.env.production?    # When the environment is production
    storage :fog              # Using fog gem to use a cloud storage service to store images separately from our application
  else 
    #storage :fog 
    storage :file             # storage :file, uses the local filesystem for storing the images, which isn’t a good practice in production.
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Add a white list of extensions which are allowed to be uploaded.
  def extension_whitelist
    %w(jpg jpeg gif png)  # verifies that the image filename ends with a valid image extension (PNG, GIF, and both variants of JPEG)
                          # %w makes an array
  end
end

 