module SessionsHelper
  
  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # Remembers a user in a persistent session.
  def remember(user)
    user.remember   # calls the remember() methodin user.rb, this generates an token and token_digest and stores it in the database
    
    
    # Here their is changed method calls cookies() => permanent() => signed()
    # permanenet() => We need to store the user_id and the user_token both permanently to remember the user
    # signed() => instead of placing the plain user_id we encrypt it
    
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  # Returns the current logged-in user (if any) or returns the user corresponding to the remember token cookie.
  # First check if the user has a temorary session session[:user_id] is nil ?
  # Else check if the user has a cookie with a user_id. If yes then
  # find the user in the database by User.find_by(id: user_id)
  # if the user is present in the database check if the user had a persistant session
  # by checking if the cookies[:remember_token] exists in the remember_digest column for that user
  # this is done by user.authenticated?(cookies[:remember_token]) method defined in user.rb
  # if yes log_in(user) 
  
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end
  
  # Logs out the current user.
  def log_out
    forget(current_user)  # forgets the users persistent session by deleting cookies and deleting the users remember_digest from the database
    session.delete(:user_id)  # delete the user id from the session 
    @current_user = nil # sets the current user to nil
  end
  
  # Forgets a persistent session.
  def forget(user)
    user.forget     # calls user.forget() which deletes the users remember_digest from the database
    cookies.delete(:user_id)  # deletes the user_id from the cookie
    cookies.delete(:remember_token) # deletes the remember_token from the cookie
  end
  
  # Returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end
  
  # Redirects to stored location (or to the default).
  # After user logs in redirect him to his requested url or default url if there is no such requested url in session[:forwarding_url]
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url) # delete the forwarding url placed in the session to avoid forwarding on subsequent login attempts
  end

  # Stores the URL trying to be accessed.
  # If an user is not logged in and tries to access edit page then store this request so that
  # the user is redirected to his requested edit page after he logs in
  # this request is stored in the session[:forwarding_url], the request will only be stored if its a get request => if request.get?
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

  
end
