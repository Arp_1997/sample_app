module UsersHelper
  
  # Gravatar is a free service that allows users to upload images and associate them with email addresses they control. 
  # As a result, Gravatars are a convenient way to include user profile images without going through the trouble of managing image upload, 
  # cropping, and storage; all we need to do is construct the proper Gravatar image URL using the user’s email address and the corresponding 
  # Gravatar image will automatically appear.
  # Gravatar URLs are based on an MD5 hash of the user’s email address
  def gravatar_for(user, size: 80 )
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")  # This will generate and return an <img> tag with the gravatar image for the user
  end
end

