class Micropost < ApplicationRecord
    
  # Using the belongs_to/has_many association Rails constructs the methods as...
  # Instead of Micropost.create we have user.microposts.create
  # Instead of Micropost.create! we have user.microposts.create!
  # Instead of Micropost.new we have user.microposts.build
  
  # Because of this association we can write this...
  # @user = users(:michael); @micropost = Micropost.new(content: "Lorem ipsum", user_id: @user.id)
  # as this: @user = users(:michael);  @micropost = @user.microposts.build(content: "Lorem ipsum") 
  
  # micropost.user	Returns the User object associated with the micropost
  # user.microposts	Returns a collection of the user’s microposts
  # user.microposts.create(arg)	Creates a micropost associated with user
  # user.microposts.create!(arg)	Creates a micropost associated with user (exception on failure)
  # user.microposts.build(arg)	Returns a new Micropost object associated with user
  # user.microposts.find_by(id: 1)	Finds the micropost with id 1 and user_id equal to user.id  
    
  
  belongs_to :user  # A MICROPOST CAN BELONG TO A USER
  
  has_many :likes, dependent: :destroy
  has_many :hearts, dependent: :destroy
  has_many :dislikes, dependent: :destroy
  
  default_scope -> { order(created_at: :desc) }         # Sets the default order in which elements are retrieved from the database as descending 
                                                        # Here we specify to fetch the latest microposts first hence we order them by their creation time
                                                        
  mount_uploader :picture, PictureUploader              # tells CarrierWave to associate the image with a model using the mount_uploader method, 
                                                        # which takes as arguments a symbol representing the attribute and the class name of the generated uploader
  
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size                               
  
  
   private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
    
end
