class Relationship < ApplicationRecord
  
  belongs_to :follower, class_name: "User"    # Each follower relationship belongs to a User
  belongs_to :followed, class_name: "User"    # Each followed relationship belongs to a User
  validates :follower_id, presence: true      # Validates a relationship must have a follower_id
  validates :followed_id, presence: true      # Validates a relationship must have a follower_id
  
end
