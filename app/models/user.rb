class User < ApplicationRecord
  
  has_many :microposts , dependent: :destroy # The 'has_many' association defines that a user has many microposts
                                             # This works because by convention  Rails looks for a Micropost model corresponding to the :microposts symbol
                                             # Here the option 'dependent: :destroy' arranges for the dependent microposts to be destroyed when the user itself is destroyed. 
                                             # This prevents userless microposts from being stranded in the database when admins choose to remove users from the system.
                                             # The microposts table has a user_id attribute to identify the user to which a micropost belongs thus user_id works as a foreign key
                                             # Rails infers the association automatically as: by default, Rails expects a foreign key of the form <class>_id, 
                                             # where <class> is the lower-case version of the class name 'user' in this case.
  
  
  # Here we define that a user has many active_relationships  
  # We define the relationship on class = "Relationship" model / here the relationships are shown as 
  # a table of the form (id , follower_id , followed_id , created_at , updated_at) where 
  # follower_id = ID of the user following other user & followed_id = ID of the user being followed by other user
  # HERE following means "the users I am following"  AND  follwers mean "the users who are following me"
  # We define active relationship as the relation of "a user following other users"
  # and passive relatioship as the relation of "a user being followed by other users"
                                             
  has_many :active_relationships, class_name:  "Relationship",      # Unlike in case of microposts where the model name was "microposts" so "has_many :microposts" worked but here
                                                                    # we want to write "has_many :active_relationships" even though the underlying model is called Relationship. 
                                                                    # Thus is this case we will thus have to tell Rails the model class name to look for by class_name:  "Relationship"
                                  foreign_key: "follower_id",       # In microposts the foreign key was 'user_id' since the 'User' model exists Rails automatically infers the assciation 
                                                                    # and understood 'user_id' was the foreign key this is because Rails expects a foreign key of the form <class>_id.
                                                                    # In the present case, the foreign key "follower_id"(the user  who is following, which is linked to the user table by
                                                                    # user_id), so we have to tell that to Rails by foreign_key: "follower_id". Here the foreign_key: "follower_id" tells us
                                                                    # to which user this active relationship belongs and thus it connects the two tables (relationship and user).
                                  dependent:   :destroy             # Since destroying a user should also destroy that user’s active relationships
  
  # These assciations/relationships give rise to the following methods ...   
  
  #             Method	                                                                    Purpose
  #---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  #   active_relationship.follower	                                                        Returns the follower ( here 'active_relationship' means the relationship object returned when the relationship was created)
  #   active_relationship.followed	                                                        Returns the followed user
  #   user.active_relationships.create(followed_id: other_user.id)                        	Creates an active relationship associated with user
  #   user.active_relationships.create!(followed_id: other_user.id)                       	Creates an active relationship associated with user (exception on failure)
  #   user.active_relationships.build(followed_id: other_user.id)	                          Returns a new Relationship object associated with user
  
  
  has_many :passive_relationships, class_name:  "Relationship",     # Now we define the other passive relatioship that is a user has many followers 
                                   foreign_key: "followed_id",      # In this case the foreign key is "followed_id" (the user  who is being followed), here foreign_key followed_id 
                                                                    # tells us to which user this passive relatioship belongs.
                                   dependent:   :destroy            # Since destroying a user should also destroy that user’s passive relationships
  

  
  
  # A has_many :through association is often used to set up a many-to-many connection with another model.
  # This association indicates that the declaring model can be matched with zero or more instances of another model by proceeding through a third model. 
  #
  # For example :
  #---------------
  # class Physician < ApplicationRecord           # A physican has many appointments and has many patients through those appointments
  #   has_many :appointments
  #   has_many :patients, through: :appointments
  # end
 
  # class Appointment < ApplicationRecord         # Each appointment physican to a pateint and a patient
  #   belongs_to :physician
  #   belongs_to :patient
  # end
 
  # class Patient < ApplicationRecord             # Each patient has many apointments and has many physicans through those appointments 
  #   has_many :appointments
  #   has_many :physicians, through: :appointments
  # end
  #
  # ------------------------------------------------------------------------------------------------
  #
  #
  # In out case each user has many active relations and has many following (which are also user's) through those active relations
  
  
  
  
  has_many :following, through: :active_relationships, source: :followed        # By default, in a has_many :through association Rails looks for a foreign key corresponding to 
                                                                                # the singular version of the association. That is in 'has_many :followeds, through: :active_relationships' 
                                                                                # Rails would see “followeds” and use the singular “followed”.
                                                                                # Since user.followeds is rather awkward, so we’ll write user.following instead and explicitly tells Rails 
                                                                                # that the source of the following array is the set of followed ids by specifying 'source: :followed' 
                                                                               
                                                                                
 # The above association allows us to define a powerful combination of Active Record and array-like behavior for example
 
 # user.following                       # returns an array of users our user is following ...
 
 # user.following.include?(other_user)  # Checks if user is following 'other_user' 
 
 # user.following.find(other_user)      # Checks if user is following 'other_user' 
 
 # user.following << other_user         # Insert 'other_user' to 'user's' following thus now
                                        # 'user' is following 'other_user'
 
 # user.following.delete(other_user)    # delete 'other_user' from 'user's' following thus 
                                        # 'user' is no longer following 'other_user'
                                        
 # Example of query: 
 # user.following << User.find(3) -> INSERT INTO "relationships" ("follower_id", "followed_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["follower_id", 1], ["followed_id", 3], ["created_at", "2019-04-16 06:45:43.618362"], ["updated_at", "2019-04-16 06:45:43.618362"]]
 
  
  # Similarly each user has many Passive relations and has many followers (which are also user's) through those passive relations
  
  has_many :followers, through: :passive_relationships, source: :follower  
  
  # This again allows us to write code like user.followers which returns an array of user's followers (user whoa are following 'user')
                                                                                
                                                                                
  # Thus in the "active_relationship",foreign_key:"follower_id" to link relationship table to user table and establish "following": that is a user has many other users whom he may follow 
  # and in the "passive_relationship",foreign key:"followed_id" to link relationship table to user table and establish "followed": that is a user has many followers
                                  
                                  
  
  attr_accessor :remember_token, :activation_token, :reset_token
  before_create :create_activation_digest                                       # Before creating/ signing up a new user create an activation digest for him
  before_save   :downcase_email                                                 # we’ll use a before_save callback to downcase the email attribute before saving the user.
  after_create :follow_admin!                                                   # After a user logs in he will automatically follow the admin user if he is not the admin himself
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,presence: true, length: { maximum: 255 } 
  validates :email, presence: true, length: { maximum: 255 },format: { with: VALID_EMAIL_REGEX },uniqueness: { case_sensitive: false }
  
  # These are just named parameters passed to the method validates like
  # validates(:email,presence:true,length:{maximum:255},format:{with:{VALID_EMAIL_REGEX}},uniqueness:{case_sensitive:false})
  
  has_secure_password # https://api.rubyonrails.org/classes/ActiveModel/SecurePassword/ClassMethods.html
  
  
  validates :password, presence: true, length: {minimum: 6}, allow_nil: true  # This allows users to edit their profile without necessarily editing their passwords
                                                                              # But new users cannot leave passwor blank as the has_secure_password includes a separate
                                                                              # presence validation that specifically catches nil passwords.
                                                                              
                                                                              
                                                                              
  class << self
    
    # Returns the hash digest of the given string.
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                 BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    # Returns a random token.
    def new_token
      SecureRandom.urlsafe_base64
    end
  end  
  
  
  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token  # returns a new random 22 character string to be used as token
    update_attribute(:remember_digest, User.digest(remember_token)) # save the digest value for the token in the database in the "remember_digest" attribute/column
                                                                    # just like we stored the digest value of the user's password
  end
  
  
  # Returns true if the given token matches the digest.
  # THIS authenticate? METHOD WORKS FOR AUTHENTICATING REMEMBER DIGEST , ACTIVATION DIGEST , PASSWORD RESET DIGEST BEACUSE OF RUBYS ABILITY OF DYNAMIC PROGRAMMING USING send("#{attribute}_digest")
  def authenticated?(attribute, token)
    
    digest = send("#{attribute}_digest")    # when attribute = "remember" => send("rememeber_digest") 
                                            # when attribute = "activation" => send("activation_digest") 
                                            # when attribute = "password" =>  send("password_digest")
    
    return false if digest.nil?                                         # This is done to avoid problems when user logs into same account from different browsers
    
    BCrypt::Password.new(digest).is_password?(token)                    # Takes a 'remember_token' and compares it with self.remember_digest
                                                                        # that is the remember_digest stored in the database for the user(self)
                                                                        # Also note the use of the remember_digest attribute, which is the same 
                                                                        # as self.remember_digest and, like name and email , is created automatically 
                                                                        # by Active Record based on the name of the corresponding database column
  end
  
  
  # Forgets a user.
  # Delete the user's remember_digest from the database
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Activates an account.
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  
  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago # Password reset sent earlier than two hours ago
  end
  
  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
    Micropost.where("user_id = ?", id)  # Returns all the micropost's of the current user
                                        # This is same as writing 'user.microposts' or where user is
                                        # the current user, since here self = current user we can just
                                        # write 'microposts' instead of 'Micropost.where("user_id = ?", id)'
  end
  
  
  # Returns a user's status feed containing an array of all microposts of the user's followings and the user's  own microposts
  def feed
    following_ids = "SELECT followed_id FROM relationships WHERE  follower_id = :user_id" # Set up a subquery
    
    Micropost.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id)   # Executes a Query like...
                                                                                          # SELECT * FROM microposts WHERE user_id IN (SELECT followed_id FROM relationships WHERE  follower_id = <follower_id>) OR user_id = <user id>
                                                                                          # This type of technique using a subquery is efficient when the number of microposts 
                                                                                          # and user's followers in very large because here the subquery data is to be pushed into the database, 
                                                                                          # which is more efficient rather than being handled in memory.
  end                                                                                     # Thus it returns all microposts of the user's followings and the user's  own microposts                                                         
                                                                            

  
  
  # In these methods following means user.following here self = user in user.rb so it works...
  
  # Follows a user.
  def follow(other_user)
    following << other_user # Insert 'other_user' to 'user's' following
                            # Backgroud Quey: INSERT INTO "relationships" ("follower_id", "followed_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["follower_id", 1], ["followed_id", 2], ["created_at", "2018-06-14 14:28:04.929149"], ["updated_at", "2018-06-14 14:28:04.929149"]]
  end

  # Unfollows a user.
  def unfollow(other_user)
    unless other_user.admin?       # User cannot unfollow admin
      following.delete(other_user) # Delete 'other_user' from 'user's' following
                                   # Backgroud Query: DELETE FROM "relationships" WHERE "relationships"."follower_id" = ? AND "relationships"."followed_id" = 2  [["follower_id", 1]]
    end
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)  # Checks if user is following 'other user'
                                    # here it looks like rails might have to pull all the followed users out of the database to apply the include? method,
                                    # but in fact for efficiency Rails arranges for the comparison to happen directly in the database similar to the user.microposts.count method.
                                    # Backgroud Query: SELECT  1 AS one FROM "users" INNER JOIN "relationships" ON "users"."id" = "relationships"."followed_id" WHERE "relationships"."follower_id" = ? AND "users"."id" = ? LIMIT ?  [["follower_id", 1], ["id", 2], ["LIMIT", 1]]
  end
  
  def follow_admin!                 # After a user logs in he will automatically follow the admin user
    admin_user = User.find_by email: "arpanghoshal3@gmail.com"
    unless self.admin?              # Admin must not follow admin
      following << admin_user
    end
  end
  
  
  private

    # Converts email to all lower-case.
    def downcase_email
      self.email.downcase!
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
    
    
                    
end
