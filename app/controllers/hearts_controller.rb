class HeartsController < ApplicationController
    
  before_action :find_post
  
  before_action :find_heart, only: [:destroy]
  
  def find_heart
   @heart = @micropost.hearts.find(params[:id])
  end

  
  def create
    if !already_hearted?
       @micropost.hearts.create(user_id: current_user.id)
    end 
    
    redirect_to root_path
    
  end
    
  

  def destroy
    if already_hearted?
      @heart.destroy
    end
  
    redirect_to root_path
  
  end
  
  def already_hearted?
    Heart.where(user_id: current_user.id, micropost_id:params[:micropost_id]).exists?
  end
  
  
  private
  
  def find_post
    @micropost = Micropost.find(params[:micropost_id])
  end
  
end
