class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:index,:edit, :update, :destroy, :following, :followers]
                                                                          # Only allow access to edit ,update, delete, index,following and followers pages if user is logged in
                                                                          # When ever a user tries on visit the edit pages before_action calls the logged_in_user method
                                                                          # If the user is not logged in the logged_in_user method will rediredt the user to the login page with a message
                                                        
  before_action :correct_user, only: [:edit, :update]   # Ensures that a user can edit or update data only if he is the correct user
                                                        # That is other users cannot edit data for a user. Only the user who is logged in
                                                        # can edit/update HIS data
                                                        # Since ANY logged in user can visit the index page so this check is not required on index page
                                                        
  
  before_action :admin_user, only: :destroy   # This will check if the current user is an admin before performing any destory or delete operation otherwise it will redirect to Home
  
  
  def new
    @user = User.new
  end
  
  def index
     @users = User.where(activated: true).paginate(page: params[:page])   # We show only users whose account is activated by checking User.where(activated: true)
                                                                          # Instead of User.all we use  User.paginate to display users in pages 
  end
  
  def show
    @user = User.find(params[:id])
    redirect_to root_url unless @user.activated?
    @microposts = @user.microposts.paginate(page: params[:page])
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      
      @user.send_activation_email # this calls the method 'send_activation_email' in models/user.rb
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
      
    else
      render 'new'
    end
  end
  
  
  def edit
    @user = User.find(params[:id])
  end
  
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params) # Update user information according to parameters passed using update_attributes() method
     flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  
  def destroy                     # deletes the selected user from the database , flashes a message and redirects to the index page again 
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
    
  # Confirms the correct user.
  # This is used to check that the user's data that one is trying to edit/update is the current user logged in
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)  # Here @user holds the user data we are trying to change , so check if @user ==  current_user
  end
    
  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
    
    
  def following
    @title = "Following"                                    # Set the titile to Following | Ruby on Rails Tutorial
    @user  = User.find(params[:id])                         # Find the user whos following we wish to see
    @users = @user.following.paginate(page: params[:page])  # Retrieve the user's followings in paginated form
    render 'show_follow'                                    # render them on the user's following page, unlike other actions
                                                            # which implicitly render the template corresponding to an action
                                                            # Here we have an explicit call to render 'show_follow' because 
                                                            # the ERb is nearly identical for the two cases
  end

  def followers
    @title = "Followers"                                    # Set the titile to Followers | Ruby on Rails Tutorial
    @user  = User.find(params[:id])                         # Find the user whos followers we wish to see
    @users = @user.followers.paginate(page: params[:page])  # Retrieve the user's followers in paginated form
    render 'show_follow'                                    # render them on the user's followers page, unlike other actions
                                                            # which implicitly render the template corresponding to an action
                                                            # Here we have an explicit call to render 'show_follow' because 
                                                            # the ERb is nearly identical for the two cases
  end                                                       


  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)  # Admin is not in the list of permitted attributes. 
                                                                                      # This is what prevents arbitrary users from granting themselves administrative access to our application.
    end

    
end