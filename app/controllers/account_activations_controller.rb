class AccountActivationsController < ApplicationController
  
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])    # If the user is not nil AND the user is not already activated AND the user's activation token is authentic
      user.activate                                                                 # Activate user
      log_in user                                                                   # Log in the user
      flash[:success] = "Account activated!"                                        # flash a "Account activated!" message
      redirect_to user                                                              # Redirect to the user's profile page
    else                                                                            # else
      flash[:danger] = "Invalid activation link"                                    # flash a "Invalid activation link" message
      redirect_to root_url                                                          # Redirect to the home page
    end
  end
  
end
