class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  private

    # Confirms a logged-in user.
    # Since we need this method to check if the user is logged in for both user_controller (to allow user's to change user data only when logged in)
    # and microposts_controller ( to allow users to create and delete microposts only when logged in), thus due to this repeated common usage
    # we keep this method in Application controller, which is the base class of all controllers
    
    def logged_in_user
      unless logged_in?
        store_location                    # store any url the user requested so that he can be redirected to it after login
        flash[:danger] = "Please log in." # display a flash message asking the user to login
        redirect_to login_url             # redirect to login page
      end
    end
    
end