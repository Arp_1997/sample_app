class RelationshipsController < ApplicationController
  
  before_action :logged_in_user             # Before logging in a user cannot create(follow) or destroy(unfollow) relations

  def create
    @user = User.find(params[:followed_id])  # Find the user to be followed
    current_user.follow(@user)               # Call the follow method on current_user to created a following relationship with the 'user'
    
    respond_to do |format|                  # Uses an AJAX request via javascript in create.js.erb to update HTML page with the unfollow partial 
      format.html { redirect_to @user }     # after successfull following without having to refresh the page
      format.js
    end                       
    
  end

  def destroy
    @user = Relationship.find(params[:id]).followed  # Find the user to be unfollowed
    current_user.unfollow(@user)                     # Call the unfollow method on current_user to destroy a following relationship with the 'user'
    
    respond_to do |format|                   # Uses an AJAX request via javascript in destroy.js.erb to update HTML page with the follow partial 
      format.html { redirect_to @user }      # after successfull unfollowing without having to refresh the page
      format.js
    end
  end
end