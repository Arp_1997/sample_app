class PasswordResetsController < ApplicationController
  
  # Because the existence of a valid @user is needed in both the edit and update actions, we’ll put the code to find and validate it in a couple of before filters
  
  before_action :get_user,   only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]    # Before editing or updating check if the password reset link has expired
  
  def new
  end
  
  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Email sent with password reset instructions"
      redirect_to root_url
    else
      flash.now[:danger] = "Email address not found"
      render 'new'
    end
  end

  def edit
  end
  
  def update
    if params[:user][:password].empty?                  # If the password given by the user is empty
      @user.errors.add(:password, "can't be empty")     # Add an error directly to the @user object’s error messages using errors.add
      render 'edit'                                     # Render the password edit page again
      
    elsif @user.update_attributes(user_params)          # If updating the user's password was successfull
      log_in @user                                      # Login the user
      @user.update_attribute(:reset_digest, nil)        # After updating password the password Digest for the user is cleared, this prevent malicious use of the reset digest later
      flash[:success] = "Password has been reset."      # Flash a success message
      redirect_to @user                                 # Redirect to users profile page
    else
      render 'edit'                                     # Else if updating the user's password was unsuccessfull render epassword edit page again with errors
    end
  end
  
  private
  
    def user_params
      params.require(:user).permit(:password, :password_confirmation)   # Returns the new password entered by the user to be updated int eh database after proper validations
    end

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # Confirms a valid user.
    def valid_user
      unless (@user && @user.activated? &&
              @user.authenticated?(:reset, params[:id]))
        redirect_to root_url
      end
    end
    
     # Checks expiration of reset token.
    def check_expiration
      if @user.password_reset_expired?              # Calls the password_reset_expired? method from user.rb on the user to check if the password reset link has expired
        flash[:danger] = "Password reset has expired."
        redirect_to new_password_reset_url
      end
    end
    
end
