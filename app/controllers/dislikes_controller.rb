class DislikesController < ApplicationController
  
  before_action :find_post
  
  before_action :find_dislike, only: [:destroy]
  
  def find_dislike
   @dislike = @micropost.dislikes.find(params[:id])
  end

  
  def create
    if !already_disliked?
       @micropost.dislikes.create(user_id: current_user.id)
    end 
    
    redirect_to root_path
    
  end
    
  

  def destroy
    if already_disliked?
      @dislike.destroy
    end
  
    redirect_to root_path
   
  end
  
  def already_disliked?
    Dislike.where(user_id: current_user.id, micropost_id:params[:micropost_id]).exists?
  end
  
  
  private
  
  def find_post
    @micropost = Micropost.find(params[:micropost_id])
  end
  
end
