
class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]              # Allow user to create and delete microposts only if he is logged in
  
  before_action :correct_user,   only: :destroy                         # checks that the current user actually has a micropost with the given id
                                                                        # before calling destroy on the micropost, this also ensures that a different
                                                                        # user cannot delete other user's microposts

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []                    # on failed micropost submission, the Home page expects an @feed_items instance variable, suppress the feed entirely by assigning it an empty array
      render 'static_pages/home'
    end
  end

  def destroy
                                                  # The micropost to delete is already assigned to the '@micropost' instance variable by the correct_user() method 
                                                  # called by before_action filter when user issued a delete request
                                                  
    @micropost.destroy                            # Destroy the selected micropost
    flash[:success] = "Micropost deleted"         # show success message
    
    redirect_to request.referrer || root_url      # Since a users's microposts appear on both is profile and home page we use the the request.referrer() method
                                                  # which redirects back to the page issuing the delete request in both cases (from profile as well as home page of user)
                                                  # If the referring URL is nil (as is the case inside some tests) it sets the root_url as the default using the || operator
                                                  # From rails 5 we can use 'redirect_back(fallback_location: root_url)' instead of request.referrer with ||
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content, :picture) # micropost_params, which permits only the micropost’s content attribute and picture if any to be modified through the web.
    end
    
    def correct_user  # set the micropost to delete in @micropost if it is present
    
      @micropost = current_user.microposts.find_by(id: params[:id])   # This verify's that the micropost's id selected for deletion belongs to the current user,
                                                                      # thus we prevent other users from deleting different user's microposts.
                                                                      # Here current_user.microposts returns all microposts of the current user
                                                                      # then we find the micropost to delete using its id with find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?                         # if there are no such microposts then redirect to user profile else set the micropost to delete in @micropost
                                                                      
    end
end