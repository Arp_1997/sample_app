class SessionsController < ApplicationController
  def new
  end
  
  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      
      if @user.activated?  # Allow login only if user is activated
        log_in @user
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)   # calls remember method in sessions_helper.rb if the rememeber me checkbox is selected
        redirect_back_or @user # this will redirect the user to any url he had requested before logging in (like editing user data) otherwise it will redirect user to his profile
        else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
      
      else
        # Create an error message.
        flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
        render 'new'
    end
  end
  
  def destroy
    # you must log out only if logged_in? is true to avoid problems when user logs in to 
    # same account in diffrerent browser windows and logs out in one window then how he again log out in the other window
    log_out if logged_in?  #session.delete(:user_id)   delete the user id from the session 
    redirect_to root_url  # go back to home
  end

end
